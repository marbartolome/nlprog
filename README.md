# nlprog

`nlprog` (for "natural language programming") is a command line application that attempts to transform natural language into code.

    $ python nlprog.py "lisp hello world"
    (defun hello-world (url stream)
    (http:with-successful-response (stream :text)
        (princ "hello world" stream)))

They way it works is by naively spitting out the first block of code of the first Stack Overflow result that comes out of a Google search for your natural language query. 

Provide a `--url` flag to get the link to the SO thread:

    $ python nlprog.py "python boilerplate" --url
    From: http://stackoverflow.com/questions/7898049/boilerplate-code-in-python
    if __name__ == "__main__":
    print "Hello, Stack Overflow!"

    for i in range(3):
        print i

    exit(0)
    
# Does this really work?
    
To test nlprog's capabilities I am going to try to put together a FizzBuzz program
in a language that I have no familiarity with. Let's try Perl.
    
I first need a sequence of numbers for 1 to 100:

    $ python nlprog.py "perl sequence of numbers"
    my $g0to9 = '{'.join(',', '0'..'9').'}';
    my $gAtoZ = '{'.join(',', 'A'..'Z').'}';
    my %glob = join('', $g0to9 x 3, $gAtoZ, $g0to9 x 4);
    while (my $_ = glob($glob)) {
    ...
    }

Close enough. I will also need to loop over the sequence:

    $ python nlprog.py "perl foreach loop"
    foreach my $var1 (@list) {
        my $var1_scratch = $var1;
        

And I need to know how to make comparisons:

    $ python nlprog.py "perl test equality" 
    "foo" == "bar";   # True, both strings evaluate to 0.
    "foo" eq "bar";   # False, the strings are not equivalent.
    "Foo" eq "foo";   # False, the F characters are different cases.
    "foo" eq "foo";   # True, both strings match exactly.
    
And if-else statements:
    
    $ python nlprog.py "perl if else"
    my @errors;
    while(<LOGFILE>) {
        if (/UNSUCCESSFUL/) {
            push(@errors, "ERROR in line $.\n") if (/Error/);
        }
    }

    if (@errors) {
        print for @errors;
    } else {
        print "script successful\n";
    }

That's nice, but do we have some kind of "else if"?

    $ python nlprog.py "perl else if" 
    ......@ (that's supposed to be a tumbleweed)

Hm, ok, so nothing was found... Let's try to word it differently:

    $ python nlprog.py "perl elseif"
    ......@ (that's supposed to be a tumbleweed)

    $ python nlprog.py "perl elsif"
    elsif ($idcount > 1 && somevariable = 0)

There you go! And we'll need to do modulos:

    $ python nlprog.py "perl modulo"
    use POSIX "fmod";

    sub foo {
        my $n1 = shift;
        my $n2 = shift;
        print "perl's fmod=" . fmod($n1,$n2), "\n";
        my $res = $n1 / $n2;
        my $t = int($res);
        print "my div=$t", "\n";
        $res = $res - $t;
        $res = $res * $n2;
        print "my mod=" . $res . "\n\n";
    }

    foo( 3044.952963, 7.1 );
    foo( 3044.952963, -7.1 );
    foo( -3044.952963, 7.1 );
    foo( -3044.952963, -7.1 );


Ok, that's a lot of clutter, but I will try with that `fmod` and see what happens. Finally, let's see how to print:

    $ python nlprog.py "perl print"
    print "Number of lines: $nids\n";
    print "Content: $ids\n";
    
So knowing this, we can put together something like this:

    use POSIX "fmod";

    foreach my $num (0..100) {
        if (fmod($num,15) == 0) {
            print "FizzBuzz\n";
        } 
        elsif (fmod($num,3) == 0) {
            print "Fizz\n";
        } 
        elsif (fmod($num,5) == 0) {
            print "Buzz\n";
        } else {
            print "$num\n";
        }
    }

Will it run?

    $ perl fizz.pl 
    FizzBuzz
    1
    2
    Fizz
    4
    Buzz
    Fizz
    7
    8
    Fizz
    Buzz
    11
    Fizz
    13
    14
    FizzBuzz
    16
    17
    Fizz
    ...
    
Success! =D

# Planned features

- Provide language as an extra parameter. This will allow to include natural language text of the response to be included as code comments.
- Traverse to next Google result if no code was found in the first.
- Interactive mode that will allow to traverse to the next block of code if you don't like the first one.
