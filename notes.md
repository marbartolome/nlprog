# scrapy

- scrapy-how-to-disable-or-change-log http://stackoverflow.com/a/17034448
- http://stackoverflow.com/questions/15611605/how-to-pass-a-user-defined-argument-in-scrapy-spider
- http://stackoverflow.com/questions/13437402/how-to-run-scrapy-from-within-a-python-script?rq=1

- http://doc.scrapy.org/en/0.16/topics/selectors.html
- http://doc.scrapy.org/en/latest/intro/overview.html
- http://doc.scrapy.org/en/latest/intro/tutorial.html

- http://pypix.com/python/build-website-crawler-based-upon-scrapy/

# xpath

- http://stackoverflow.com/questions/5033955/xpath-select-text-node

# other

- http://love-python.blogspot.co.uk/2008/07/strip-html-tags-using-python.html
