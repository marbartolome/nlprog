from scrapy import cmdline
import google
import argparse
import sys

def main():
    # Args parsing
    parser = argparse.ArgumentParser(description='Transforms natural language into code')
    parser.add_argument('query', type=str,
                    help='natural language description of the code you want')
    parser.add_argument('--url', dest='show_url', action='store_true',
                    help='show the stackoverflow url where code was fetched from')
    args = parser.parse_args()
    
    # Get first SO result from google
    try:
        so_url = google.search("{} site:stackoverflow.com".format(args.query)).next()
        if args.show_url: print "From:", so_url
    except StopIteration:
        print "No results! =("
        sys.exit(0)

    # Scrape code
    cmdline.execute("scrapy crawl so -s LOG_ENABLED=0 -a so_url={}".format(so_url).split())

if __name__ == '__main__':
  main()