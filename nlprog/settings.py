# Scrapy settings for nlprog project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'nlprog'

SPIDER_MODULES = ['nlprog.spiders']
NEWSPIDER_MODULE = 'nlprog.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'nlprog (+http://www.yourdomain.com)'
