from scrapy.spider import Spider
from scrapy.selector import Selector

class SOSpider(Spider):
    name = 'so'
    
    def __init__(self, so_url):
        self.start_urls = [so_url]
 
    def parse(self, response):
        sel = Selector(response)
        try:
            # Note: test xpaths in chrome's js console like this: $x("//img")
            answer = sel.xpath('//td[@class="answercell"]/div[@class="post-text"]/pre/code/text()')[0]
            print answer.extract()
        except IndexError:
            print "......@ (that's supposed to be a tumbleweed)"